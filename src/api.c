/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <qosmodule/api.h>
#include <debug/sahtrace.h>
#include <amxc/amxc_macros.h>

#define QOSMODULE_ZONE "qosmodule"
#define MOD_QOS_CTRL "qos-ctrl"

static const char* api_function_names[QOS_MODULE_API_FUNC_MAX] =
{
    [QOS_MODULE_API_FUNC_ACTIVATE_SHAPER] = QOS_MODULE_API_FUNC_NAME_ACTIVATE_SHAPER,
    [QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER] = QOS_MODULE_API_FUNC_NAME_ACTIVATE_SCHEDULER,
    [QOS_MODULE_API_FUNC_ACTIVATE_QUEUE] = QOS_MODULE_API_FUNC_NAME_ACTIVATE_QUEUE,
    [QOS_MODULE_API_FUNC_DEACTIVATE_SHAPER] = QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SHAPER,
    [QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER] = QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SCHEDULER,
    [QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE] = QOS_MODULE_API_FUNC_NAME_DEACTIVATE_QUEUE,
    [QOS_MODULE_API_FUNC_RETRIEVE_STATS] = QOS_MODULE_API_FUNC_NAME_RETRIEVE_STATS,
    [QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS] = QOS_MODULE_API_FUNC_NAME_RETRIEVE_QUEUE_STATS
};

const char* qos_module_get_func_name(qos_module_api_func_t id) {
    const char* name = NULL;

    when_true(id >= QOS_MODULE_API_FUNC_MAX, exit);
    name = api_function_names[id];

exit:
    return name;
}

int qos_module_register(amxm_module_t** module, qos_module_api_funcs_t* const funcs) {
    int retval = -1;
    unsigned int i = 0;
    amxm_shared_object_t* so = NULL;
    amxm_callback_t* func = NULL;

    when_null(funcs, exit);
    when_null(module, exit);
    *module = NULL;

    so = amxm_so_get_current();
    when_null(so, exit);

    retval = amxm_module_register(module, so, MOD_QOS_CTRL);
    when_failed(retval, exit);

    //Start from the first api function.
    func = &funcs->activate_shaper;

    for(i = 0; i < QOS_MODULE_API_FUNC_MAX; i++, func++) {
        if(!func || !*func) {
            SAH_TRACEZ_WARNING(QOSMODULE_ZONE, "API function %s is not set", api_function_names[i]);
            continue;
        }

        retval |= amxm_module_add_function(*module, api_function_names[i], *func);

        if(0 != retval) {
            SAH_TRACEZ_ERROR(QOSMODULE_ZONE, "Failed to register API function %s", api_function_names[i]);
            goto exit;
        }
    }

exit:
    return retval;
}

int qos_module_deregister(amxm_module_t** module) {
    int retval = -1;
    unsigned int i = 0;

    when_null(module, exit);
    when_null(*module, exit);
    retval = 0;

    for(i = 0; i < QOS_MODULE_API_FUNC_MAX; i++) {
        retval |= amxm_module_remove_function(*module, api_function_names[i]);

        if(0 != retval) {
            SAH_TRACEZ_ERROR(QOSMODULE_ZONE, "Failed to deregister API function %s", api_function_names[i]);
            //continue.
        }
    }

    retval |= amxm_module_deregister(module);
    if(0 != retval) {
        SAH_TRACEZ_ERROR(QOSMODULE_ZONE, "Failed to deregister module");
    }

exit:
    return retval;
}

int qos_module_exec_api_function(const qos_module_api_func_t id, const char* const module_name,
                                 amxc_var_t* args, amxc_var_t* ret) {
    int retval = -1;

    when_true(id >= QOS_MODULE_API_FUNC_MAX, exit);
    when_str_empty(module_name, exit);
    when_null(args, exit);

    retval = amxm_execute_function(module_name, MOD_QOS_CTRL, api_function_names[id], args, ret);
    if(0 != retval) {
        SAH_TRACEZ_ERROR(QOSMODULE_ZONE, "API function %s failed for module %s, retval: %d",
                         api_function_names[id], module_name, retval);
    }

exit:
    return retval;
}

