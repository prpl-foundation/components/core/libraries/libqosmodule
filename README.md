# libqosmodule

[TOC]

# Introduction

`libqosmodule` is a library that serves three purposes:
1. Provide the required dispatching functions for the QoS low-level API.
2. Provide helper functions to ease the implementation of QoS modules.
3. Provide helper functions for the tr181-qos component used to invoke a
   low-level QoS module API function.

To know the position of this library in the overall QoS subsystem, the following
image provides some clarification.

![Overview](doc/images/libqosmodule_overview.svg)

From top to bottom, these components can be seen:

- The TR-181 Device.QoS data model.
- tr181-qos: the userspace application, taking care of the data model
  configuration.
- libqoscommon: provides general functions for tr181-qos and libqosnode.
- libqosnode: an additional layer, abstracting the TR-181 data model and used
  to interface with the QoS modules.
- libqosmodule: this library. It's main task is to provide helper functions
  that can be used by the modules or the tr181-qos component to ease the
  implementation of the QoS low-level API.

# Trace zone

Add trace zone `qosmodule` to see traces in the log files.

# Building, installing and testing

## Docker container

You could install all tools needed for testing and developing on your local
machine, or use a pre-configured environment. Such an environment is already
prepared for you as a docker container.

1. Install Docker

    Docker must be installed on your system. Here are some links that could
    help you:

    - [Get Docker Engine - Community for
      Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for
      Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for
      Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for
      CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
      <br /><br />

    Make sure you user id is added to the docker group:

        sudo usermod -aG docker $USER
    <br />

2. Fetch the container image

    To get access to the pre-configured environment, pull the image and launch
    a container.

    Pull the image:

        docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    Before launching the container, you should create a directory which will be
    shared between your local machine and the container.

        mkdir -p ~/amx/qos/modules/

    Launch the container:

        docker run -ti -d \
            --name oss-dbg \
            --restart=always \
            --cap-add=SYS_PTRACE \
            --sysctl net.ipv6.conf.all.disable_ipv6=1 \
            -e "USER=$USER" \
            -e "UID=$(id -u)" \
            -e "GID=$(id -g)" \
            -v ~/amx:/home/$USER/amx \
            registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    The `-v` option bind mounts the local directory for the amx project in
    the container, at the exact same place.
    The `-e` options create environment variables in the container. These
    variables are used to create a user name with exactly the same user id and
    group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

        docker exec -ti --user $USER oss-dbg /bin/bash

## Building

### Prerequisites

<code>libqosmodule</code> depends on the following libraries:

- libsahtrace

These libraries can be installed in the container:

    sudo apt-get install mod-sahtrace sah-lib-sahtrace-dev iproute2

The `iproute2`is installed for later purposes and provides the `ip` command.

### Build libqosmodule

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    just created for the ambiorix project and clone this library in it (on your
    local machine).

        cd ~/amx/qos
        mkdir libraries
        cd libraries
        git clone git@gitlab.com:prpl-foundation/components/core/libraries/libqosmodule.git

2. Build it

    When using the internal gitlab, you must define an environment variable
    `VERSION_PREFIX` before building in your docker container.

        export VERSION_PREFIX="master_"

    After the variable is set, you can build the package.

        cd ~/amx/qos/modules/libqosmodule
        make

## Installing

### Using make target install

You can install your own compiled version easily in the container by running
the install target.

    cd ~/amx/qos/libraries/libqosmodule
    sudo -E make install

### Using package

From within the container you can create packages.

    cd ~/amx/qos/libraries/libqosmodule
    make package

The packages generated are:

    ~/amx/qos/libraries/libqosmodule/libqosmodule-<VERSION>.tar.gz
    ~/amx/qos/libraries/libqosmodule/libqosmodule-<VERSION>.deb

You can copy these packages and extract/install them.

For Ubuntu or Debian distributions use dpkg:

    sudo dpkg -i ~/amx/qos/libraries/libqosmodule/libqosmodule-<VERSION>.deb

## Testing

### Prerequisites

No extra components are needed for testing `libqosmodule`.

### Run tests

You can run the tests by executing the following command:

    cd ~/amx/qos/libraries/libqosmodule/test
    make

Or this command if you also want the coverage tests to run:

    cd ~/amx/qos/libraries/libqosmodule/tests
    make run coverage

You can combine both commands:

    cd ~/amx/qos/libraries/libqosmodule
    make test

### Coverage reports

The coverage target will generate coverage reports using
[gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and
[gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each c-file is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are
available in the output directory of the compiler used.  For example, when using
native gcc, the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML
coverage reports can be found at
`~/amx/qos/libraries/libqosmodule/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser. In the container start a
python3 http server in background.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/modules/libqosmodule/output/<MACHINE>/coverage/report

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/libraries/libqosmodule$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/qos/libraries/libqosmodule/output/x86_64-linux-gnu/coverage/report/`

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths

The documentation is split in two parts, starting from the repository path:

    cd ~/amx/qos/libraries/libqosmodule

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.

The code itself is documented in the approriate header and source files.
Documentation is only generated for the header files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/amx/qos/libraries/libqosmodule
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser. As described in the coverage reports
section, you can start a http server in the container.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/libraries/libqosmodule/output/html/index.html

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/libraries/libqosmodule$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/qos/libraries/libqosmodule/output/html/index.html`

# What's next?

Consult the full documentation for a complete description of the QoS low-level API.

