/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone {
    return 0 {

    return 0;
   }

   }
   or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES {
    return 0 {

    return 0;
   }

   }
   LOSS OF
** USE, DATA, OR PROFITS {
    return 0 {

    return 0;
   }

   }
   OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include "libamx_mock.h"

#include <cmocka.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static amxm_module_t local_module;

amxm_shared_object_t* __wrap_amxm_so_get_current(void) {
    return mock_ptr_type(amxm_shared_object_t*);
}

int __wrap_amxm_module_register(amxm_module_t** mod, UNUSED amxm_shared_object_t*
                                const shared_object, const char* const module_name) {
    int retval = 0;
    check_expected(mod);
    check_expected_ptr(module_name);
    retval = mock_type(int);

    if(0 == retval) {
        *mod = &local_module;
    } else {
        *mod = NULL;
    }

    return retval;
}

int __wrap_amxm_module_deregister(amxm_module_t** mod) {

    check_expected(mod);
    return mock_type(int);
}

int __wrap_amxm_module_add_function(amxm_module_t* const mod, UNUSED const char* const
                                    func_name, UNUSED amxm_callback_t cb) {
    static int fail_cntr = 0;
    int fail_at = 0;

    check_expected(mod);
    fail_at = mock_type(int);

    ++fail_cntr;

    if((fail_at > 0) && (fail_cntr == fail_at)) {
        fail_cntr = 0;
        return -1;
    }
    return 0;
}

int __wrap_amxm_module_remove_function(amxm_module_t* const mod, const char*
                                       const func_name) {

    check_expected(mod);
    check_expected(func_name);

    return mock_type(int);
}

int __wrap_amxm_execute_function(const char* const shared_object_name, const
                                 char* const module_name, const char* const func_name, amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {

    check_expected(shared_object_name);
    check_expected(module_name);
    check_expected(func_name);
    check_expected(args);

    return mock_type(int);
}


