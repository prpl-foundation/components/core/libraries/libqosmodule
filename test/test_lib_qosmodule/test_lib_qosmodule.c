/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>

#include "test_lib_qosmodule.h"
#include "libamx_mock.h"

#include <qosmodule/api.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static amxm_module_t local_module;
static amxm_shared_object_t so;

void test_qosmodule_function_names(UNUSED void** state) {
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_ACTIVATE_SHAPER, "activate-shaper");
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_ACTIVATE_SCHEDULER, "activate-scheduler");
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_ACTIVATE_QUEUE, "activate-queue");
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SHAPER, "deactivate-shaper");
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SCHEDULER, "deactivate-scheduler");
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_DEACTIVATE_QUEUE, "deactivate-queue");
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_RETRIEVE_STATS, "retrieve-stats");
    assert_string_equal(QOS_MODULE_API_FUNC_NAME_RETRIEVE_QUEUE_STATS, "retrieve-queue-stats");

    assert_int_equal(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER, 0);
    assert_int_equal(QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER, 1);
    assert_int_equal(QOS_MODULE_API_FUNC_ACTIVATE_QUEUE, 2);
    assert_int_equal(QOS_MODULE_API_FUNC_DEACTIVATE_SHAPER, 3);
    assert_int_equal(QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER, 4);
    assert_int_equal(QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE, 5);
    assert_int_equal(QOS_MODULE_API_FUNC_RETRIEVE_STATS, 6);
    assert_int_equal(QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS, 7);
}

void test_qosmodule_api_funcs_offset(UNUSED void** state) {
    qos_module_api_funcs_t funcs;
    const uintptr_t base_ptr = (uintptr_t) &funcs;
    const unsigned int plen = sizeof(amxm_callback_t*);

    assert_int_equal(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER * plen, (uintptr_t) &funcs.activate_shaper - base_ptr);
    assert_int_equal(QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER * plen, (uintptr_t) &funcs.activate_scheduler - base_ptr);
    assert_int_equal(QOS_MODULE_API_FUNC_ACTIVATE_QUEUE * plen, (uintptr_t) &funcs.activate_queue - base_ptr);
    assert_int_equal(QOS_MODULE_API_FUNC_DEACTIVATE_SHAPER * plen, (uintptr_t) &funcs.deactivate_shaper - base_ptr);
    assert_int_equal(QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER * plen, (uintptr_t) &funcs.deactivate_scheduler - base_ptr);
    assert_int_equal(QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE * plen, (uintptr_t) &funcs.deactivate_queue - base_ptr);
    assert_int_equal(QOS_MODULE_API_FUNC_RETRIEVE_STATS * plen, (uintptr_t) &funcs.retrieve_statistics - base_ptr);
    assert_int_equal(QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS * plen, (uintptr_t) &funcs.retrieve_queue_statistics - base_ptr);
}

static int api_call_stub(UNUSED const char* const function_name, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    return 0;
}

void test_qosmodule_register(UNUSED void** state) {
    int retval = -1;
    amxm_module_t* module = NULL;
    qos_module_api_funcs_t funcs = {0};

    //Parameter checking.
    retval = qos_module_register(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_module_register(&module, NULL);
    assert_int_equal(retval, -1);

    retval = qos_module_register(NULL, &funcs);
    assert_int_equal(retval, -1);

    //amxm_so_get_current returns NULL.
    will_return(__wrap_amxm_so_get_current, cast_ptr_to_largest_integral_type(NULL));
    retval = qos_module_register(&module, &funcs);
    assert_int_equal(retval, -1);
    assert_null(module);

    //amxm_so_get_current returns a valid pointer.
    //amxm_module_register returns an error.
    will_return(__wrap_amxm_so_get_current, cast_ptr_to_largest_integral_type(&so));
    expect_any(__wrap_amxm_module_register, mod);
    expect_string(__wrap_amxm_module_register, module_name, "qos-ctrl");
    will_return(__wrap_amxm_module_register, -1);
    retval = qos_module_register(&module, &funcs);
    assert_int_equal(retval, -1);
    assert_null(module);

    //Success, but no functions are registered.
    will_return_always(__wrap_amxm_so_get_current, cast_ptr_to_largest_integral_type(&so));
    expect_string(__wrap_amxm_module_register, module_name, "qos-ctrl");
    expect_any_always(__wrap_amxm_module_register, mod);
    will_return(__wrap_amxm_module_register, 0);
    retval = qos_module_register(&module, &funcs);
    assert_int_equal(retval, 0);
    assert_non_null(module);

    //Assign a stub function to all API function pointers.
    funcs.activate_shaper = api_call_stub;
    funcs.activate_scheduler = api_call_stub;
    funcs.activate_queue = api_call_stub;
    funcs.deactivate_shaper = api_call_stub;
    funcs.deactivate_scheduler = api_call_stub;
    funcs.deactivate_queue = api_call_stub;
    funcs.retrieve_statistics = api_call_stub;
    funcs.retrieve_queue_statistics = api_call_stub;

    //Fail after 4 added API functions.
    expect_string(__wrap_amxm_module_register, module_name, "qos-ctrl");
    will_return_always(__wrap_amxm_module_register, 0);
    expect_any_always(__wrap_amxm_module_add_function, mod);
    will_return_count(__wrap_amxm_module_add_function, 4, 4);
    retval = qos_module_register(&module, &funcs);
    assert_int_equal(retval, -1);

    //Fail after 6 added API functions.
    expect_any_always(__wrap_amxm_module_register, module_name);
    will_return_count(__wrap_amxm_module_add_function, 6, 6);
    retval = qos_module_register(&module, &funcs);
    assert_int_equal(retval, -1);

    //Success
    will_return_always(__wrap_amxm_module_add_function, 0);
    retval = qos_module_register(&module, &funcs);
    assert_int_equal(retval, 0);
    assert_non_null(module);
}

void test_qosmodule_deregister(UNUSED void** state) {
    int retval = -1;
    amxm_module_t* module = NULL;

    expect_any_always(__wrap_amxm_module_remove_function, mod);
    expect_any_always(__wrap_amxm_module_remove_function, func_name);
    expect_any_always(__wrap_amxm_module_deregister, mod);

    //Parameter checking
    retval = qos_module_deregister(NULL);
    assert_int_equal(retval, -1);

    retval = qos_module_deregister(&module);
    assert_int_equal(retval, -1);

    module = &local_module;
    //Fail QOS_MODULE_API_FUNC_MAX times in a row, the number of API functions.
    will_return_count(__wrap_amxm_module_remove_function, -1, QOS_MODULE_API_FUNC_MAX);
    will_return(__wrap_amxm_module_deregister, 0);
    retval = qos_module_deregister(&module);
    assert_int_equal(retval, -1);

    //Pass QOS_MODULE_API_FUNC_MAX times in a row, the number of API functions.
    will_return_count(__wrap_amxm_module_remove_function, 0, QOS_MODULE_API_FUNC_MAX);
    //Fail here ...
    will_return(__wrap_amxm_module_deregister, -1);
    retval = qos_module_deregister(&module);
    assert_int_equal(retval, -1);

    //Success.
    will_return_count(__wrap_amxm_module_remove_function, 0, QOS_MODULE_API_FUNC_MAX);
    will_return(__wrap_amxm_module_deregister, 0);
    retval = qos_module_deregister(&module);
    assert_int_equal(retval, 0);
}

void test_qosmodule_exec_api_function(UNUSED void** state) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    //Parameter checking.
    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER, NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_MAX, NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_MAX + 1, NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER, "", NULL, NULL);
    assert_int_equal(retval, -1);

    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER, "mod-qos-test", NULL, NULL);
    assert_int_equal(retval, -1);

    expect_any_always(__wrap_amxm_execute_function, shared_object_name);
    expect_any_always(__wrap_amxm_execute_function, module_name);
    expect_any_always(__wrap_amxm_execute_function, func_name);
    expect_any_always(__wrap_amxm_execute_function, args);
    amxc_var_init(&data);
    amxc_var_init(&ret);

    will_return(__wrap_amxm_execute_function, -1);
    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER, "mod-qos-test", &data, NULL);
    assert_int_equal(retval, -1);

    will_return(__wrap_amxm_execute_function, 0);
    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER, "mod-qos-test", &data, NULL);
    assert_int_equal(retval, 0);

    will_return(__wrap_amxm_execute_function, 0);
    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER, "mod-qos-test", &data, &ret);
    assert_int_equal(retval, 0);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_qosmodule_get_func_name(UNUSED void** state) {
    assert_null(qos_module_get_func_name(QOS_MODULE_API_FUNC_MAX));
    assert_null(qos_module_get_func_name(QOS_MODULE_API_FUNC_MAX + 1));

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_ACTIVATE_SHAPER),
                        QOS_MODULE_API_FUNC_NAME_ACTIVATE_SHAPER);

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER),
                        QOS_MODULE_API_FUNC_NAME_ACTIVATE_SCHEDULER);

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_ACTIVATE_QUEUE),
                        QOS_MODULE_API_FUNC_NAME_ACTIVATE_QUEUE);

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_DEACTIVATE_SHAPER),
                        QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SHAPER);

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER),
                        QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SCHEDULER);

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE),
                        QOS_MODULE_API_FUNC_NAME_DEACTIVATE_QUEUE);

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_RETRIEVE_STATS),
                        QOS_MODULE_API_FUNC_NAME_RETRIEVE_STATS);

    assert_string_equal(qos_module_get_func_name(QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS),
                        QOS_MODULE_API_FUNC_NAME_RETRIEVE_QUEUE_STATS);
}
