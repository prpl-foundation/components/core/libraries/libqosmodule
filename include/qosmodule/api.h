/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOSMODULE_API_H__)
#define __QOSMODULE_API_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @defgroup qosmodule QoS Module API
 *
 * @brief
 * This library provides functions and data structures to get a new QoS module
 * up and running quickly.
 */

#include <amxc/amxc.h>
#include <amxm/amxm.h>


/**
 * @ingroup qosmodule
 * @{
 */
/** The name of the API function to activate a shaper. */
#define QOS_MODULE_API_FUNC_NAME_ACTIVATE_SHAPER        "activate-shaper"
/** The name of the API function to activate a scheduler. */
#define QOS_MODULE_API_FUNC_NAME_ACTIVATE_SCHEDULER     "activate-scheduler"
/** The name of the API function to activate a queue. */
#define QOS_MODULE_API_FUNC_NAME_ACTIVATE_QUEUE         "activate-queue"
/** The name of the API function to deactivate a shaper. */
#define QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SHAPER      "deactivate-shaper"
/** The name of the API function to deactivate a scheduler. */
#define QOS_MODULE_API_FUNC_NAME_DEACTIVATE_SCHEDULER   "deactivate-scheduler"
/** The name of the API function to deactivate a queue. */
#define QOS_MODULE_API_FUNC_NAME_DEACTIVATE_QUEUE       "deactivate-queue"
/** The name of the API function to query global interface statistics. */
#define QOS_MODULE_API_FUNC_NAME_RETRIEVE_STATS         "retrieve-stats"
/** The name of the API function to query interface queue statistics. */
#define QOS_MODULE_API_FUNC_NAME_RETRIEVE_QUEUE_STATS   "retrieve-queue-stats"
/** @} */

/**
 * @ingroup qosmodule
 * @brief
 * Identifiers of the different low-level API functions.
 */
typedef enum {
    /** API function identifier to activate a shaper. */
    QOS_MODULE_API_FUNC_ACTIVATE_SHAPER,
    /** API function identifier to activate a scheduler. */
    QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER,
    /** API function identifier to activate a queue. */
    QOS_MODULE_API_FUNC_ACTIVATE_QUEUE,
    /** API function identifier to deactivate a shaper. */
    QOS_MODULE_API_FUNC_DEACTIVATE_SHAPER,
    /** API function identifier to deactivate a scheduler. */
    QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER,
    /** API function identifier to deactivate a queue. */
    QOS_MODULE_API_FUNC_DEACTIVATE_QUEUE,
    /** API function identifier to retrieve global interface statistics. */
    QOS_MODULE_API_FUNC_RETRIEVE_STATS,
    /** API function identifier to retrieve interface queue statistics. */
    QOS_MODULE_API_FUNC_RETRIEVE_QUEUE_STATS,
    /** Maximum identifier. Keep this the last item in the enumeration. */
    QOS_MODULE_API_FUNC_MAX
} qos_module_api_func_t;

/**
 * @ingroup
 * Get the function name based on the identifier.
 *
 * @param[in] id The low-level API function identifier. E.g. QOS_MODULE_API_FUNC_ACTIVATE_SHAPER.
 */
const char* qos_module_get_func_name(qos_module_api_func_t id);

/**
 * @ingroup qosmodule
 * @brief
 * Data structure containing function pointers to the module's API functions.
 *
 * The prototype for each API function is:
 * @code
 * typedef int (* amxm_callback_t)(const char* const function_name, amxc_var_t* args, amxc_var_t* ret);
 * @endcode
 * The API function has the following arguments:
 * @param[in] function_name The name of the API function.
 * @param[in] args A libamxc variant of type cstring_t, containing one QoS.Node instance path.
 * @param[out] ret A libamxc variant that can be used to return data to the caller.
 *
 * @return 0 on success, -1 on error.
 */
typedef struct {
    /** Pointer to the callback function to activate a shaper. */
    amxm_callback_t activate_shaper;
    /** Pointer to the callback function to activate a scheduler. */
    amxm_callback_t activate_scheduler;
    /** Pointer to the callback function to activate a queue. */
    amxm_callback_t activate_queue;
    /** Pointer to the callback function to deactivate a shaper. */
    amxm_callback_t deactivate_shaper;
    /** Pointer to the callback function to deactivate a scheduler. */
    amxm_callback_t deactivate_scheduler;
    /** Pointer to the callback function to deactivate a queue. */
    amxm_callback_t deactivate_queue;
    /** Pointer to the callback function to query global interface statistics. */
    amxm_callback_t retrieve_statistics;
    /** Pointer to the callback function to query interface queue statistics. */
    amxm_callback_t retrieve_queue_statistics;
} qos_module_api_funcs_t;

/**
 * @ingroup qosmodule
 *
 * This function can be used to register a new QoS module. For the caller, it
 * is important to keep a pointer to an amxm_module_t structure.
 *
 * For the API functions, it is not required to set all of them. A subset is also valid.
 *
 * @param[inout] module Double pointer to the amxm_module_t structure.
 * @param[in] funcs Pointer to the data structure containing all API function pointers.
 *
 * @return 0 on success, otherwise -1.
 */
int qos_module_register(amxm_module_t** module, qos_module_api_funcs_t* const funcs);

/**
 * @ingroup qosmodule
 *
 * Deregister a QoS module.
 *
 * @param[inout] module Double pointer to the amxm_module_t structure.
 *
 * @return 0 on success, otherwise -1.
 */
int qos_module_deregister(amxm_module_t** module);

/**
 * @ingroup qosmodule
 *
 * Use this function to execute a low-level API function, provided by a QoS module.
 *
 * @param[in] id The low-level API function identifier. E.g. QOS_MODULE_API_FUNC_ACTIVATE_SHAPER.
 * @param[in] module_name The name of the registered QoS module. E.g. "mod-qos-tc".
 * @param[in] args The arguments to be passed to the API function.
 * @param[out] ret A variant that can be used by the API function to return data to the caller.
 *
 * @attention
 * The parameters `args` and `ret` (variants) must be initialized by the caller.
 *
 * @return 0 on success, otherwise -1.
 */
int qos_module_exec_api_function(const qos_module_api_func_t id, const char* const module_name,
                                 amxc_var_t* args, amxc_var_t* ret);

#ifdef __cplusplus
}
#endif

#endif // __QOSMODULE_API_H__
